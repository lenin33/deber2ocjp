package com.company;

/**
 * Created by lenin on 15/05/17.
 */
public interface Figura {

    final double  pi = 3.14;

    public double area();
    public double perímetro();
}
