package com.company;

/**
 * Created by lenin on 15/05/17.
 */
public class Triangulo implements Figura{

    tiposTriangulo tipos;
    private double lado;

    public Triangulo(tiposTriangulo tipos, double lado) {
        this.tipos = tipos;
        this.lado = lado;
    }

    @Override
    public double area() {
        switch(tipos){
            case ESCALENO:
                return Math.sqrt(3)/4*Math.pow(lado,2);

            case ISOSCELES:
                return 0;

            case EQUILATERO:
                return 0;

                default:
                    return 0;
        }
    }

    @Override
    public double perímetro() {
        return 0;
    }
}
