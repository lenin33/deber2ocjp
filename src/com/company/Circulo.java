package com.company;

/**
 * Created by lenin on 15/05/17.
 */
public class Circulo implements Figura{

    private double radio;

    @Override
    public double area() {
        return pi*radio*radio;
    }

    @Override
    public double perímetro() {
        return 2*pi*radio;
    }
}

