package com.company;

/**
 * Created by lenin on 15/05/17.
 */
public class Cuadrado implements Figura{

    private double lado;

    public Cuadrado(double lado) {
        this.lado = lado;
    }

    @Override
    public double area() {
        return lado*lado;
    }

    @Override
    public double perímetro() {
        return 4*lado;
    }
}
