package com.universidad;

/**
 * Created by lenin on 15/05/17.
 */
public class estudiante extends Persona{

    String numeroUnico;
    String facultad;

    public estudiante(String nombre, String apellido, char sexo, String numeroUnico, String facultad) {
        super(nombre, apellido, sexo);
        this.numeroUnico = numeroUnico;
        this.facultad = facultad;
    }

    public estudiante(String numeroUnico, String facultad) {
        this.numeroUnico = numeroUnico;
        this.facultad = facultad;
    }

    @Override
    public String obtenerDatos() {
        return numeroUnico + " " + facultad + "\n";
        //return super.obtenerDatos();
    }
}
