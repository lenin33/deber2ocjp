package com.universidad;

/**
 * Created by lenin on 15/05/17.
 */
public class Persona {
    private String nombre;
    private String apellido;
    private  char sexo;

    public Persona(String nombre, String apellido, char sexo) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.sexo = sexo;
    }

     public Persona() {
    }

    public String obtenerDatos(){

        return nombre+" "+ apellido+ " " + sexo + "\n";

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }
}
